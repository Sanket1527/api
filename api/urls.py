
from django.contrib import admin
from django.urls import path
from api_app import views

urlpatterns = [
    # path('admin/', admin.site.urls),
    # path('employees/',views.employee_api),
    # path('employees/<int:pk>',views.employee_api),
    path('get_employees/',views.get_employee,name='employees'),
    path('create/',views.create_employee,name='create_employee'),
    path('Update_Employee/<int:pk>/',views.Update_Employee,name="Update_Employee"),
    path('Partial_Update_Employee/<int:pk>/',views.Partial_Update_Data,name="Partial_Update_Data"),
    path('delete/<int:pk>/',views.delete_employee,name="delete_employee"),

]
