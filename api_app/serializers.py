from rest_framework import serializers
from .models import employee

class employeeserializer(serializers.ModelSerializer):
    class Meta:
        model = employee
        fields = ['id','Name','Address','Phone','Email','Department','Gender']

    def validate_Phone(self,value):
        if value.isnumeric(): 
            return value
        else:
            raise serializers.ValidationError('Invalid Number')

    def validate_Name(self, value):
        """
        Check that value is a valid name.
        """
        value=value.isalpha()
        # print(value)
        if value == False:
            raise serializers.ValidationError("Please enter full name") 
        return value
        
    def validate_Email(self,value): 
        if "@gmail.com" in value: 
            return value 
        else: 
            raise serializers.ValidationError("This field accepts mail id of google only") 