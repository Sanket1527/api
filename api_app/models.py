from django.db import models

# Create your models here.
class employee(models.Model):
    Name = models.CharField(max_length=255)
    Address = models.CharField(max_length=255)
    Phone = models.CharField(max_length=225)
    Email = models.EmailField(max_length=255)
    Department = models.CharField(max_length=255)
    Gender = models.CharField(max_length=255)
