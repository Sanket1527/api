from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import employee
from .serializers import employeeserializer

# Create your views here.
 
# @api_view(['GET','POST','PUT','PATCH','DELETE'])
# def employee_api(request,pk=None):
#     if request.method == 'GET':
#         id= pk
#         if id is not None: 
#             emp = employee.objects.get(id=id)
#             serializer = employeeserializer(emp)
#             return Response(serializer.data)

#         emp = employee.objects.all()
#         serializer = employeeserializer(emp, many=True)
#         return Response(serializer.data)
    
#     if request.method == 'POST':
#         serializer = employeeserializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response({'msg':'data created'})
#         return Response(serializer.errors)
    
#     if request.method == 'PUT':
#         id = pk
#         emp = employee.objects.get(pk=id)
#         serializer = employeeserializer(emp, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response({'msg':'Complete Data Updated'})
#         return Response(serializer.errors)
    
#     if request.method == 'PATCH':
#         id = pk
#         emp = employee.objects.get(pk=id)
#         serializer = employeeserializer(emp, data=request.data,partial=True)
#         if serializer.is_valid():
#             serializer.save()
#             return Response({'msg':'Partial Data Updated'})
#         return Response(serializer.errors)

#     if request.method == 'DELETE':
#         id = pk
#         emp = employee.objects.get(pk=id)
#         emp.delete()
#         return Response({'msg':'Data delete'})


@api_view(['GET'])
def get_employee(request,pk=None):
    id=pk
    if id is not None:
        emp = employee.objects.get(id=id)
        serializer = employeeserializer(emp)
        return Response(serializer.data)
    emp = employee.objects.all()
    serializer = employeeserializer(emp,many = True)
    return Response(serializer.data)

@api_view(['POST'])
def create_employee(request):
    serializer = employeeserializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response({'msg':'Data Created'})
    return Response(serializer.errors)

@api_view(['GET','PUT'])
def Update_Employee(request,pk):
    if request.method == 'PUT':
        emp1 = employee.objects.get(pk=pk)
        serializer2 = employeeserializer(emp1, data=request.data)
        if serializer2.is_valid():
            serializer2.save()
            return Response({'msg':'Complete Data Updated'})
    emp = employee.objects.get(pk=pk)
    serializer = employeeserializer(emp)
    return Response(serializer.data) 
             
@api_view(['PATCH'])
def Partial_Update_Data(request,pk):
    emp = employee.objects.get(pk=pk)

    serializer = employeeserializer(emp, data=request.data,partial=True)
    if serializer.is_valid():
        serializer.save()
        return Response({'msg':'Partial Data Updated'})
    return Response(serializer.errors)

@api_view(['DELETE'])
def delete_employee(request,pk):
    emp = employee.objects.get(pk=pk)
    emp.delete()
    return Response({'msg':'Data delete'})